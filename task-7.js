// #1
//  Напишіть код, який буде присвоювати змінній "result"
//  значення суми змінних "x" та "y" - у випадку якщо x < y, 
//  різницю "x" и "y" - у випадку якщо x > y, 
// та їх добуток у решті випадків.
// x = 10, y = 20 -> result = 30
// x = 20, y = 10 -> result = 10
// x = 10, y = 10 -> result = 100

function getResult(x, y) {
    if (x > y) {
        return x - y;
    } else if (x < y) {
        return x + y;
    } else if (x === y) {
        return x * y;
    }
}
// console.log('#1 - ' + getResult(10, 5));
// console.log('#1 - ' + getResult(10, 17));
// console.log('#1 - ' + getResult(10, 10));


// #2
// Є два масиви випадкової довжини заповнених випадковими числами. 
// Вам необхідно написати функцію, яка обчислює суму всіх елементів обох масивів.

// a = [1, 2, 3, -5, 0, 10], b = [5, -1, 7] -> 22

function getArraySum(arrayX = [], arrayY = []) {
    const arraies = arrayX.concat(arrayY);

    return arraies.reduce((accumulator, currentValue) => accumulator + currentValue);
}

// const a = [1, 2, 3, -5, 0, 10];
// const b = [5, -1, 7];

// console.log('#2 - ' + getArraySum(a, b));


// #3
// Дано масив, в якому є елементи true і false. Написати функцію,яка порахує кількість елементів true у масиві.
// countTrue([true, false, false, true, false]) ➞ 2
// countTrue([false, false, false, false]) ➞ 0
// countTrue([]) ➞ 0

const x = [true, false, false, true, false, true, true];

function countTrue(array = []) {
    let count = 0;
    array.forEach(item => {
        if (item == true) {
            count++;
        }       
    });
    return count;
}
// console.log('#3 - ' + countTrue(x));

// #4
// Напишіть функцію, яка обчислює подвійний факторіал числа Приклад:
// doubleFactorial(0) ➞ 1
// doubleFactorial(2) ➞ 2
// doubleFactorial(9) ➞ 945 // 97531 = 945
// doubleFactorial(14) ➞ 645120

function doubleFactorial(x) {
    let start;
    if (x % 2 === 0) {
        start = 2;
    } else {
        start = 1;
    }    
    let result = 1;
    for (let i = start; i <= x; i = i + 2) {
        result = result * i;
    }
    return result;
}
// console.log('#4 - ' + doubleFactorial(14));


// #5
// Дано два об’єкти person, в них містяться поля name и age.
// 5.1 Написати функцію compareAge, яка буде порівнювати два об’єкти за віком і повертати шаблонний рядок як результат.
//  Шаблонний рядок виглядає наступним чином: <Person 1 name> is <older | younger | the same age as> <Person 2 name>
// 5.2 Створити масив із декількох об’єктів person (3-5 елемнтів) і відсортувати його за віком: а) за зростанням б) за спаданням

function compareAge(person1, person2) {
    let str = '';
    
    if (person1.Age > person2.Age) {
        str = 'older';
    } else if (person1.Age < person2.Age) {
        str = 'younger';
    } else if (person1.Age == person2.Age) {
        str = 'the same age as';
    }

    return `${person1.Name} ${str} ${person2.Name}`;;
}

let persons = [
    {
        Name: 'Ivan',
        Age: 20
    },
    {
        Name: 'Mykola',
        Age: 24
    },
    {
        Name: 'Vitaliy',
        Age: 29
    },
    {
        Name: 'Oleg',
        Age: 20
    },
    {
        Name: 'Zhenya',
        Age: 17
    }
];

console.log('#5.1 - ' + compareAge(persons[0], persons[1]));
console.log('#5.1 - ' + compareAge(persons[0], persons[4]));
console.log('#5.1 - ' + compareAge(persons[0], persons[3]));

console.log(persons);

persons.sort((a, b) => a.Age - b.Age);

console.log(persons);

persons.sort((a, b) => b.Age - a.Age);

console.log(persons);
